# kasa-demo

## env variable examples:
MONGO_CONNECTION=mongodb://localhost:27017/kasa
PORT=3000 (optional - this is the default value)
HOST=localhost (optional - this is the default value)

## database seed
command: npm run seed

## dev server start:
command: npm run server

## availability check examples:
http://localhost:3000/check?startDate=2022-03-05&endDate=2022-03-07&listingName=A  
http://localhost:3000/check?startDate=2022-03-05&endDate=2022-03-07&listingName=a  
http://localhost:3000/check?startDate=2022-03-05&endDate=2022-03-07  
http://localhost:3000/check?startDate=2022-03-05&endDate=2022-03-12&listingName=A  

## technical details:
I decided to use express, with mongodb, 
Steps to install the app: (requires node 12 or newer)
1. run "npm install" -> for the node modules
2. set the environment variables for mongo and the express server in the ".env" file
3. run "npm run seed" to create a few record
4. run "npm run dev" to start the express server

I used node's eventEmitter class to emulate events.
the events are in the "src/server.js" file

The bulk of the code in "src/bookingService.js" file
the "init" function run the test commands (recommendations, create, remove)

My search logic has 1 difference from the examples:
if there is one listing from example 2022-03-01 to 2022-03-04 in that case
I'm blocking the end date too for new bookings.
It was not clear for me how to handle the reserved case when the new booking's end date match an already saved booking's start date, so I decided to block every matching date/range.

## question -  answers

How would you support blocking a unit between two dates for a period of time?
>> I would add "type" to bookings, for example normal bookings could have the type "guest",
and maintenance could be "service" with different date checking mechanism

How would you handle room types when inquiring about availability? 
What (if at all) would you change in your service or your data structure?  
>> At the moment Listings have a "name" in the schema, I would extend this with "type"
so the search can be modified according to listing types

How would you handle date change recommendations?
>> I implemented a recommendation function which can be called like this:  
const recommendations = await getRecommendations({  
  startDate: '2022-03-05',  
  endDate: '2022-04-010', 
  listingName: 'A'  
})  
it does create date-pairs for possible listings,
example:
let's say between two dates (10 days) there is two booking for 3 days
0001100100 | 0 - free 1 - reserved, 
in that case the function will create 3 booking recommendation for the  
  3 days at the start  
  2 days between the bookings  
  2 days at the end  

