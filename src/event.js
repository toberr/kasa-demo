let event = require('events')
let eventEmitter = new event.EventEmitter()

const events = {
  BookingConfirmed: 'BookingConfirmed',
  BookingCanceled: 'BookingCanceled',
}

module.exports = {
  events,
  eventEmitter,
}
