const {eventEmitter, events} = require('./event')
const Booking =  require('./models/Booking')
const Listing =  require('./models/Listing')

async function check({startDate, endDate, listingName}) {
  let listingRecord
  
  // param validation
  for (const key of ['startDate', 'endDate', 'listingName']) {
    if (
      typeof arguments[0][key] !== 'string'
    ) {
      return {
        success: false,
        msg: `${key} has to be a string`
      }
    }
  }
  
  if (startDate > endDate) {
    return {
      success: false,
      msg: 'startDate has to be smaller then endDate'
    }
  }
  
  const condition = {
    storno: false,
    $and: [
      {
        $or: [
          {
            $and: [
              { 'startDate': { $gte: startDate } }, 
              { 'startDate': { $lte: endDate } }
            ]
          },
          {
            $and: [
              { 'endDate': { $gte: startDate } }, 
              { 'endDate': { $lte: endDate } }
            ]
          },
        ]
      }
    ]
  }
  
  if (listingName) {
    listingRecord = await Listing.findOne({
      name: listingName
    }).exec()
    
    if (!listingRecord) {
      return {
        success: false,
        msg: `no listing found with ${listingName}`
      }
    }
    condition.listing = listingRecord._id
  }
  
  const bookingRecords = await Booking.find(condition).exec()
  
  return {
    success: true,
    bookingRecords,
    listingRecord,
  }
}

async function create(params) {
  const temp = await check(params)
  
  if (!temp.success) {
    return temp
  }
  
  if (temp.bookingRecords && temp.bookingRecords.length) {
    return {
      success: false,
      msg: 'There is already a booking between these dates.'
    }
  }
  
  const newBooking = new Booking({ 
    name: params.name,
    startDate: params.startDate,
    endDate: params.endDate,
    listing: temp.listingRecord._id
  })
  
  const newBookingRecord = await newBooking.save()
  eventEmitter.emit(events.BookingConfirmed, {
    bookingId: newBookingRecord._id.toString(),
    listingId: temp.listingRecord._id.toString(),
    startDate: params.startDate,
    endDate: params.endDate
  })
  
  return newBookingRecord
}

async function remove(cond) {
  const doc = await Booking.findOne(cond).exec()
  if (!doc) {
    return
  }
  doc.storno = true
  await doc.save()
  
  eventEmitter.emit(events.BookingCanceled, {
    bookingId: doc.id,
    listingId: doc.listing.toString(),
    startDate: doc.startDate,
    endDate: doc.endDate
  })
  
  return doc
}

async function getRecommendations({startDate, endDate, listingName}) {
  const temp = await check({ startDate, endDate, listingName })
  const bookings = temp.bookingRecords.map(x => ({
    startDate: x.startDate.toISOString().split('T')[0],
    endDate: x.endDate.toISOString().split('T')[0]
  }))

  let cursor = startDate
  let days = []

  while (cursor <= endDate) {
    const day = { date: cursor}
    
    for (const booking of bookings) {
      day.state = (
        cursor >= booking.startDate && 
        cursor <= booking.endDate
      ) ? false : true
      if (!day.state) {
        break
      }
    }
    days.push(day)
    
    cursor = new Date(Date.parse(new Date(cursor)) + (3600 * 1000 * 24))
    cursor = cursor.toISOString().split('T')[0]
  }

  let recommendations = []
  let listing = {
    startDate: null,
    endDate:  null
  }
  days.forEach((item, index) => {
    if (item.state && !listing.startDate) {
      listing.startDate = item.date
    }
    if (
      item.state &&
      listing.startDate && 
      (
        !days[index +1] ||
        (days[index +1] && !days[index +1].state)
      )
    ) {
      listing.endDate = item.date
      recommendations.push({...listing})
      listing.startDate = null
      listing.endDate = null
    }
  })
  return {
    recommendations,
    currentBookings: bookings
  }
}

async function init () {
  console.log('Booking service inited')
  
  const recommendations = await getRecommendations({
    startDate: '2022-03-05',
    endDate: '2022-04-05',
    listingName: 'A'
  })
  console.log('recommendations between 2022-03-05 and 2022-04-05', recommendations)
  
  const result1 = await create({
    name: 'D',
    startDate: '2022-03-10',
    endDate: '2022-03-15',
    listingName: 'A'
  }) 
  console.log('result 2022-03-10 -> 2022-03-15', result1)
  
  const result2 = await create({
    name: 'D',
    startDate: '2022-04-20',
    endDate: '2022-04-25',
    listingName: 'A'
  }) 
  console.log('result 2022-04-20 -> 2022-04-25', result2)
  
  //const removedDoc = await remove({name: 'D'})
}

module.exports = {
  init,
  getRecommendations,
  check,
  create,
  remove,
}
