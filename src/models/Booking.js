const {Schema, mc} = require('../db')

const bookingSchema = new Schema({
  name: String,
  startDate: Date,
  endDate: Date,
  storno: {type: Boolean, default: false},
  listing: {type : Schema.ObjectId, ref : 'Listing'}
})
const Booking = mc.model('Booking', bookingSchema)

module.exports = Booking
