const { Schema, mc } = require('../db')

const listingSchema = new Schema({ 
  name: String,
  storno: {type: Boolean, default: false},
})
const Listing = mc.model('Listing', listingSchema)

module.exports = Listing
