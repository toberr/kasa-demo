const mongoose = require('mongoose')
const Schema = mongoose.Schema

const mc = mongoose.createConnection(
  process.env.MONGO_CONNECTION
)

module.exports = {
  mongoose,
  mc,
  Schema,
}
