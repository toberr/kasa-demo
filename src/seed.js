require('dotenv').config()
const { mc } =  require('./db')
const Listing =  require('./models/Listing')
const Booking =  require('./models/Booking')

mc.once('open', async () => {
  console.log('Connected to mongoose database')
  
  const listingA = new Listing({ name: 'A' })
  const listingARecord = await listingA.save()
  console.log('listingARecord created', listingARecord)
  
  const BookingA = new Booking({ 
    name: 'A',
    startDate: '2022-03-01',
    endDate: '2022-03-04',
    listing: listingARecord._id
  })
  const bookingARecord = await BookingA.save()
  console.log('bookingARecord created', bookingARecord)
  
  const BookingB = new Booking({
    name: 'B',
    startDate: '2022-03-08',
    endDate: '2022-03-12',
    listing: listingARecord._id
  })
  const bookingBRecord = await BookingB.save()
  console.log('bookingBRecord created', bookingBRecord)
  
  const BookingC = new Booking({
    name: 'C',
    startDate: '2022-03-17',
    endDate: '2022-03-20',
    listing: listingARecord._id
  })
  const BookingCRecord = await BookingC.save()
  console.log('BookingCRecord created', BookingCRecord)
  
  process.exit()
})
