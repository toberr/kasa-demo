require('dotenv').config()
const express = require('express')
const {eventEmitter, events} = require('./event')
const bookingService = require('./bookingService')

const app = express()
const { mc } =  require('./db')
  
mc.once('open', async () => {
  console.log('Connected to mongoose database')
  
  await bookingService.init()
  
  const port = process.env.PORT || 3000
  const host = process.env.HOST || 'localhost'
  
  //app.use(express.json()) 
  
  app.get('/check', async(req, res) => {
    try {
      const {startDate, endDate, listingName} = req.query
      if (!(startDate && endDate && listingName)) {
        res.json({
          success: false,
          msg: 'startDate, endDate and listingName paramters are required.'
        })
        return
      }
      const result = await bookingService.check({
        startDate: startDate,
        endDate: endDate,
        listingName: listingName
      })
      if (result && !result.success) {
        res.json(result)
      } else if (result.bookingRecords && result.bookingRecords.length) {
        res.json({
          success: false,
          msg: `Listing '${listingName}' is not available between ${startDate} - ${endDate}`
        })
      } else {
        res.json({
          success: true,
          msg: `Listing '${listingName}' is available between ${startDate} - ${endDate}`
        })
        
      }
    } catch (error) {
      console.log('error')
      res.json({success: false, msg: 'server error'})
    }
  })

  app.listen(
    port,
    host,
    () => {
      console.log(`We are live on http://${host}:${port}`)
    })
})

//events
eventEmitter.on(events.BookingConfirmed, (e) => {
  console.log(events.BookingConfirmed, e)
})

eventEmitter.on(events.BookingCanceled, (e) => {
  console.log(events.BookingCanceled, e)
})
